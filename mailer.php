<?php
	require "vendor/autoload.php";

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	$mail = new PHPMailer(true);

    $staff_email = "youcheese.com@gmail.com";
    $staff_password = "capstone2";

    $subject = "Aboitizland - Loan Calculator Results";
    
    $tcpHouseholdIncome = htmlspecialchars($_POST["tcpHouseholdIncome"]);
    $tcpInterestRate = htmlspecialchars($_POST["tcpInterestRate"]);
    $tcpLoanTerm = htmlspecialchars($_POST["tcpLoanTerm"]);
    $tcpDownPayment = htmlspecialchars($_POST["tcpDownPayment"]);
    $tcpBalance = htmlspecialchars($_POST["tcpBalance"]);
    $tcpMonthlyAmortization = htmlspecialchars($_POST["tcpMonthlyAmortization"]);
    $tcpFactorRate = htmlspecialchars($_POST["tcpFactorRate"]);
    $tcpTotalContractPrice = htmlspecialchars($_POST["tcpTotalContractPrice"]);
    $maTotalContractPrice = htmlspecialchars($_POST["maTotalContractPrice"]);
    $maReservationFee = htmlspecialchars($_POST["maReservationFee"]);
    $maInterestRate = htmlspecialchars($_POST["maInterestRate"]);
    $maDownPayment = htmlspecialchars($_POST["maDownPayment"]);
    $maDownPaymentTerm = htmlspecialchars($_POST["maDownPaymentTerm"]);
    $maBalance = htmlspecialchars($_POST["maBalance"]);
    $maBalanceTerm = htmlspecialchars($_POST["maBalanceTerm"]);
    $maMonthlyDownPayment = htmlspecialchars($_POST["maMonthlyDownPayment"]);
    $maMonthlyAmortization = htmlspecialchars($_POST["maMonthlyAmortization"]);
    $maHouseholdIncome = htmlspecialchars($_POST["maHouseholdIncome"]);

    $userName = htmlspecialchars($_POST["userName"]);
    $userEmail = htmlspecialchars($_POST["userEmail"]);
    $userNumber = htmlspecialchars($_POST["userNumber"]);

    $tcpBody = "";
    $maBody = "";

    $nameBody = "<div>Hi, " . $userName . "!" . $userNumber . "</div>";

    if($_POST["tcpExist"] == 1){
        // INTEGER VALIDATION
        if(is_numeric($tcpHouseholdIncome) && is_numeric($tcpInterestRate) && is_numeric($tcpLoanTerm) && is_numeric($tcpDownPayment) && is_numeric($tcpBalance) && is_numeric($tcpMonthlyAmortization) && is_numeric($tcpFactorRate) && is_numeric($tcpTotalContractPrice)){
            $tcpBody = "
                <table>
                    <tr>
                        <th colspan='2'>Total Contract Price</th>
                    </tr>
    
                    <tr>
                        <td>Monthly Household Income</td>
    
                        <td>
                            " . number_format($tcpHouseholdIncome , 2) . "
                        </td>
                    </tr>
    
                    <tr>
                        <td>Interest Rate</td>
    
                        <td>
                            " . $tcpInterestRate * 100 . "%
                        </td>
                    </tr>
    
                    <tr>
                        <td>Loan Term</td>
    
                        <td>
                            " . $tcpLoanTerm . " Years
                        </td>
                    </tr>
    
                    <tr>
                        <td>Down Payment</td>
    
                        <td>
                            " . $tcpDownPayment . "%
                        </td>
                    </tr>
    
                    <tr>
                        <td>Balance</td>
                        
                        <td>
                            " . $tcpBalance * 100 . "%
                        </td>
                    </tr>
    
                    <tr>
                        <td>Capacity to Pay / Monthly Amortization</td>
    
                        <td>
                            " . number_format($tcpMonthlyAmortization , 2) . "
                        </td>
                    </tr>
    
                    <tr>
                        <td>Factor Rate</td>
    
                        <td>
                            " . number_format($tcpFactorRate , 11) . "
                        </td>
                    </tr>
    
                    <tr>
                        <td>Total Contract Price</td>
    
                        <td>
                            " . number_format($tcpTotalContractPrice , 2) . "
                        </td>
                    </tr>
                </table>
            ";
        }else{
            die();
        }
    }
    
    if($_POST["maExist"] == 1){
        // INTEGER VALIDATION
        if(is_numeric($maTotalContractPrice) && is_numeric($maReservationFee) && is_numeric($maInterestRate) && is_numeric($maDownPayment) && is_numeric($maDownPaymentTerm) && is_numeric($maBalance) && is_numeric($maBalanceTerm) && is_numeric($maMonthlyDownPayment) && is_numeric($maMonthlyAmortization) && is_numeric($maHouseholdIncome)){
            $maBody = "
                <table>
                    <tr>
                        <th colspan='2'>Capacity to Pay / Monthly Amortization</th>
                    </tr>
    
                    <tr>
                        <td>Total Contract Price</td>
    
                        <td>
                            " . number_format($maTotalContractPrice , 2) . "
                        </td>
                    </tr>
    
                    <tr>
                        <td>Reservation Fee</td>
    
                        <td>
                            " . number_format($maReservationFee , 2) . "
                        </td>
                    </tr>
    
                    <tr>
                        <td>Interest Rate</td>
    
                        <td>
                            " . $maInterestRate * 100 . "%
                        </td>
                    </tr>
    
                    <tr>
                        <td>Down Payment</td>
    
                        <td>
                            " . $maDownPayment . "%
                        </td>
                    </tr>
    
                    <tr>
                        <td>Down Payment Term</td>
    
                        <td>
                            " . $maDownPaymentTerm . " Months
                        </td>
                    </tr>
    
                    <tr>
                        <td>Balance</td>
                        
                        <td>
                            " . $maBalance * 100 . "%
                        </td>
                    </tr>
    
                    <tr>
                        <td>Balance Term</td>
                        
                        <td>
                            " . $maBalanceTerm . " Years
                        </td>
                    </tr>
    
                    <tr>
                        <td>Monthly Down Payment</td>
    
                        <td>
                            " . number_format($maMonthlyDownPayment , 2) . "
                        </td>
                    </tr>
    
                    <tr>
                        <td>Monthly Amortization</td>
    
                        <td>
                            " . number_format($maMonthlyAmortization , 2) . "
                        </td>
                    </tr>
    
                    <tr>
                        <td>Monthly Household Income Required</td>
    
                        <td>
                            " . number_format($maHouseholdIncome , 2) . "
                        </td>
                    </tr>
                </table>
            ";
        }else{
            die();
        }
    }

	try {
		// $mail->SMTPDebug = 4;
		$mail->isSMTP();
		$mail->Host = "smtp.gmail.com";
		$mail->SMTPAuth = true;
		$mail->Username = $staff_email;
		$mail->Password = $staff_password;
		$mail->SMTPSecure = "tls";
		$mail->Port = 587;

		$mail->setFrom($staff_email, "Aboitizland");
		$mail->addAddress($userEmail);

		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $nameBody . $tcpBody . $maBody;

        $mail->send();
        
        echo "Message sent.";
	} catch (Exception $e) {
		echo "Message could not be sent. Mailer Error: ", $mail->ErrorInfo;
	}

	die();
?>