$(document).ready(function(){
    /*************** Prevent Non-numerical Characters ***************/
    $(this).on('keydown', 'input[type="number"]', function(e){
        if (e.which != 8 && e.which != 0 && e.which < 48 || e.which > 57){
            e.preventDefault()
        }
    })
    /****************************************************************/

    /******************** Default Interest Rates ********************/
    var interestDivisor = 250
    /****************************************************************/

    /*********************** To Determine TCP ***********************/
    var $tcpHouseholdIncome = $('#tcp-household-income'),
        $tcpInterestRate = $('#tcp-interest-rate'),
        $tcpLoanTerm = $('#tcp-loan-term'),
        $tcpDownPayment = $('#tcp-down-payment'),
        $tcpBalance = $('#tcp-balance'),
        $tcpMonthlyAmortization = $('#tcp-monthly-amortization'),
        $tcpFactorRate = $('#tcp-factor-rate'),
        $tcpTotalContractPrice = $('#tcp-total-contract-price'),
        tcpHouseholdIncomeVal = $tcpHouseholdIncome.val(),
        tcpLoanTermVal = $tcpLoanTerm.val(),
        tcpDownPaymentVal = $tcpDownPayment.val(),
        tcpBalanceVal = 0,
        tcpMonthlyAmortizationVal = 0,
        tcpFactorRateVal = 0,
        tcpTotalContractPriceVal = 0,
        tcpInterestRateVal = tcpInterestRateVal = tcpLoanTermVal / interestDivisor,
        tcpResults = 0

    // Household Income 
    $tcpHouseholdIncome.on('change', function(){
        tcpHouseholdIncomeVal = $tcpHouseholdIncome.val()
    })
    // Loan Term (in years) 
    $tcpLoanTerm.on('change', function(){
        tcpLoanTermVal = $tcpLoanTerm.val()

        tcpInterestRateVal = tcpLoanTermVal / interestDivisor

        calculateTCP()
    })
    // Down Payment (in %) 
    $tcpDownPayment.on('change', function(){
        tcpDownPaymentVal = $tcpDownPayment.val()

        calculateTCP()
    })

    function calculateTCP(){
        if(tcpHouseholdIncomeVal != 0 && tcpResults != 0){
            // Interest Rate
            $('#tcp-interest-rate-text').text((tcpInterestRateVal * 100) + '%')

            // Balance (in %)
            tcpBalanceVal = (100 - tcpDownPaymentVal) / 100
            $('#tcp-balance-text').text((tcpBalanceVal * 100) + '%')

            // Capacity to Pay/Monthly Amortization 
            tcpMonthlyAmortizationVal = tcpHouseholdIncomeVal * .3
            $('#tcp-monthly-amortization-text').text(tcpMonthlyAmortizationVal.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}))

            // Factor Rate
            tcpFactorRateVal = (tcpInterestRateVal / 12) / ((1 - Math.pow((1 + (tcpInterestRateVal / 12)) , (-tcpLoanTermVal * 12))))
            $('#tcp-factor-rate-text').text(tcpFactorRateVal)

            // TCP
            tcpTotalContractPriceVal = (tcpMonthlyAmortizationVal / ( tcpFactorRateVal * tcpBalanceVal))
            $('#tcp-total-contract-price-text').text(tcpTotalContractPriceVal.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}))
        }
    }

    // Calculate TCP
    $(this).on('click', 'button#tcp-calculate', function(){        
        if(tcpHouseholdIncomeVal != 0 && tcpResults == 0){            
            tcpResults++
        }

        // INTEGER VALIDATION
        if($.isNumeric(tcpHouseholdIncomeVal) && $.isNumeric(tcpInterestRateVal) && $.isNumeric(tcpLoanTermVal) && $.isNumeric(tcpDownPaymentVal) && $.isNumeric(tcpBalanceVal) && $.isNumeric(tcpMonthlyAmortizationVal) && $.isNumeric(tcpFactorRateVal) && $.isNumeric(tcpTotalContractPriceVal)){
            calculateTCP()

            $('#mail-results').next().text('')
        }else{
            return false
        }
    })
    /****************************************************************/

    /********** To Determine Monthly Amortization/Capacity **********/
    var $maTotalContractPrice = $('#ma-total-contract-price'),
        $maInterestRate = $('#ma-interest-rate'),
        $maReservationFee = $('#ma-reservation-fee'),
        $maDownPayment = $('#ma-down-payment'),
        $maDownPaymentTerm = $('#ma-down-payment-term'),
        $maBalance = $('#ma-balance'),
        $maBalanceTerm = $('#ma-balance-term'),
        $maMonthlyDownPayment = $('#ma-monthly-down-payment'),
        $maMonthlyAmortization = $('#ma-monthly-amortization'),
        $maHouseholdIncome = $('#ma-household-income'),
        maTotalContractPriceVal = $maTotalContractPrice.val(),
        maReservationFeeVal = $maReservationFee.val(),
        maDownPaymentVal = $maDownPayment.val(),
        maDownPaymentTermVal = $maDownPaymentTerm.val(),
        maBalanceVal = 0,
        maBalanceTermVal = $maBalanceTerm.val(),
        maMonthlyDownPaymentVal = 0,
        maMonthlyAmortizationVal = 0,
        maHouseholdIncomeVal = 0,
        maInterestRateVal = maBalanceTermVal / interestDivisor,
        maResults = 0

    // TSP/TCP
    $maTotalContractPrice.on('change', function() {
        maTotalContractPriceVal = $maTotalContractPrice.val()
    })
    // Reservation Fee
    $maReservationFee.on('change', function(){
        maReservationFeeVal = $maReservationFee.val()

        calculateMA()
    })
    // Down Payment (in %)
    $maDownPayment.on('change', function(){
        maDownPaymentVal = $maDownPayment.val()

        calculateMA()
    })
    // Down Payment Term (in months)
    $maDownPaymentTerm.on('change', function(){
        maDownPaymentTermVal = $maDownPaymentTerm.val()

        calculateMA()
    })
    // Balance Term (in years)
    $maBalanceTerm.on('change', function(){
        maBalanceTermVal = $maBalanceTerm.val()

        maInterestRateVal = maBalanceTermVal / interestDivisor

        calculateMA()
    })

    function calculateMA(){
        if(maTotalContractPriceVal != 0 && maResults != 0){
            // Interest Rate
            $('#ma-interest-rate-text').text((maInterestRateVal * 100) + '%')

            // Balance (in %)
            maBalanceVal = (100 - maDownPaymentVal) / 100
            $('#ma-balance-text').text((maBalanceVal * 100) + '%')

            // Down Payment/month
            maMonthlyDownPaymentVal = ((maTotalContractPriceVal * (maDownPaymentVal / 100) - maReservationFeeVal) / maDownPaymentTermVal)
            $('#ma-monthly-down-payment-text').text(maMonthlyDownPaymentVal.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}))

            // Amortization/Month
            maMonthlyAmortizationVal = (maInterestRateVal / 12) / ((1 - Math.pow((1 + (maInterestRateVal / 12)) , (-maBalanceTermVal * 12)))) * (maTotalContractPriceVal * maBalanceVal)
            $('#ma-monthly-amortization-text').text(maMonthlyAmortizationVal.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}))

            // Household Income Required
            maHouseholdIncomeVal = (maMonthlyAmortizationVal * 100) / 30
            $('#ma-household-income-text').text(maHouseholdIncomeVal.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}))
        }
    }
    
    // Calculate Capacity to Pay / Monthly Amortization
    $(this).on('click', 'button#ma-calculate', function(){
        if(maTotalContractPriceVal != 0 && maResults == 0){           
            maResults++
        }

        // INTEGER VALIDATION
        if($.isNumeric(maTotalContractPriceVal) && $.isNumeric(maReservationFeeVal) && $.isNumeric(maInterestRateVal) && $.isNumeric(maDownPaymentVal) && $.isNumeric(maDownPaymentTermVal) && $.isNumeric(maBalanceVal) && $.isNumeric(maBalanceTermVal) && $.isNumeric(maMonthlyDownPaymentVal) && $.isNumeric(maMonthlyAmortizationVal) && $.isNumeric(maHouseholdIncomeVal)){
            calculateMA()

            $('#mail-results').next().text('')
        }else{
            return false
        }
    })
    /****************************************************************/

    /******************* Pass calculation results *******************/
    var $userName = $('#user-name'),
        $userEmail = $('#user-email'),
        $userNumber = $('#user-number'),
        userNameVal = '',
        userEmailVal = '',
        userNumberVal = ''

    $userName.on('change', function(){
        userNameVal = $userName.val()
    })

    $userEmail.on('change', function(){
        userEmailVal = $userEmail.val()
    })

    $userNumber.on('change', function(){
        userNumberVal = $userNumber.val()
    })

    $(this).on('click', 'button#mail-results', function(){
        if(tcpResults == 1 || maResults == 1){
            if(userNameVal == ''){
                $userName.closest('div').find('span').text('Please provide a name.')
            }else{
                $userName.closest('div').find('span').text('')
            }

            if(userEmailVal == ''){
                $userEmail.closest('div').find('span').text('Please provide an email address.')
            }else{
                $userEmail.closest('div').find('span').text('')
            }

            if(userNameVal != '' && userEmailVal != ''){
                var results = {
                    url: 'mailer.php',
                    type: 'POST',
                    data: null,
                    success: function(data){
                        $('#mail-results').next().text('Email sent!')
                    }
                }
        
                if(tcpResults == 1 && maResults == 1){
                    results.data = {
                        tcpHouseholdIncome: tcpHouseholdIncomeVal,
                        tcpInterestRate: tcpInterestRateVal,
                        tcpLoanTerm: tcpLoanTermVal,
                        tcpDownPayment: tcpDownPaymentVal,
                        tcpBalance: tcpBalanceVal,
                        tcpMonthlyAmortization: tcpMonthlyAmortizationVal,
                        tcpFactorRate: tcpFactorRateVal,
                        tcpTotalContractPrice: tcpTotalContractPriceVal,
                        maTotalContractPrice: maTotalContractPriceVal,
                        maReservationFee: maReservationFeeVal,
                        maInterestRate: maInterestRateVal,
                        maDownPayment: maDownPaymentVal,
                        maDownPaymentTerm: maDownPaymentTermVal,
                        maBalance: maBalanceVal,
                        maBalanceTerm: maBalanceTermVal,
                        maMonthlyDownPayment: maMonthlyDownPaymentVal,
                        maMonthlyAmortization: maMonthlyAmortizationVal,
                        maHouseholdIncome: maHouseholdIncomeVal,
                        tcpExist: 1,
                        maExist: 1,
                        userName: userNameVal,
                        userEmail: userEmailVal,
                        userNumber: userNumberVal
                    }
                }else if(tcpResults == 1){
                    results.data = {
                        tcpHouseholdIncome: tcpHouseholdIncomeVal,
                        tcpInterestRate: tcpInterestRateVal,
                        tcpLoanTerm: tcpLoanTermVal,
                        tcpDownPayment: tcpDownPaymentVal,
                        tcpBalance: tcpBalanceVal,
                        tcpMonthlyAmortization: tcpMonthlyAmortizationVal,
                        tcpFactorRate: tcpFactorRateVal,
                        tcpTotalContractPrice: tcpTotalContractPriceVal,
                        tcpExist: 1,
                        maExist: 0,
                        userName: userNameVal,
                        userEmail: userEmailVal,
                        userNumber: userNumberVal
                    }
                }else if(maResults == 1){
                    results.data = {
                        maTotalContractPrice: maTotalContractPriceVal,
                        maReservationFee: maReservationFeeVal,
                        maInterestRate: maInterestRateVal,
                        maDownPayment: maDownPaymentVal,
                        maDownPaymentTerm: maDownPaymentTermVal,
                        maBalance: maBalanceVal,
                        maBalanceTerm: maBalanceTermVal,
                        maMonthlyDownPayment: maMonthlyDownPaymentVal,
                        maMonthlyAmortization: maMonthlyAmortizationVal,
                        maHouseholdIncome: maHouseholdIncomeVal,
                        tcpExist: 0,
                        maExist: 1,
                        userName: userNameVal,
                        userEmail: userEmailVal,
                        userNumber: userNumberVal
                    }
                }
        
                $.ajax(results)
            }
        }else{
            $('#mail-results').next().text('Please calculate something first!')
        }
    })
    /****************************************************************/
})